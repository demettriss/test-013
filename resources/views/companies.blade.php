<p>
    {{ $company->desc ?? '' }}
</p>
@foreach($companies as $f)
    <div class="card card-company company-{{ $f->uuid }}">
        <div class="card-header">
            {{ $f->title }}
            <button type="button" class="btn tx-white btn-link float-right"
                    data-uuid="{{ $f->uuid }}"
                    data-action="{{ $f->filials->count() ? 'filial' : 'user' }}"
            >
                подробнее
            </button>
        </div>
        <div class="card-body d-none"></div>
    </div>
@endforeach
