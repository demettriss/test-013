<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

        <meta name="csrf-token" content="{{ csrf_token() }}">

    </head>
    <body>
    <div class="container">
        <!-- Content here -->
    </div>

        <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

        <script>
            $(document).ready(function () {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    method: 'POST',
                    url: '{{ route('companies') }}',
                    data: {},
                    success: function(msg) {
                        $('.container').html(msg);
                    }
                });
            });

            $(document).on('click', '.card-company .card-header .btn-link', function(e) {
                e.preventDefault();
                let uuid = $(this).attr('data-uuid');
                let action = $(this).attr('data-action');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                if (action === 'filial') {
                    $.ajax({
                        method: 'POST',
                        url: '{{ route('companies') }}/' + uuid,
                        success: function(msg) {
                            $('.company-' + uuid + ' .card-body')
                                .removeClass('d-none')
                                .html(msg);
                        }
                    });
                } else if (action === 'user') {
                    $.ajax({
                        method: 'POST',
                        url: '{{ route('company.users') }}',
                        data: {'uuid' : uuid},
                        success: function(msg) {
                            $('.company-' + uuid + ' .card-body')
                                .removeClass('d-none')
                                .html(msg);
                        }
                    });
                }
            });
        </script>
    </body>
</html>
