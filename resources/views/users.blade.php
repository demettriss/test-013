<p>
    {{ $company->desc ?? '' }}
</p>
@if($company->users->count())
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Role</th>
        </tr>
        </thead>
        <tbody>
        @foreach($company->users as $u)
            <tr>
                <th scope="row">{{ $loop->iteration }}</th>
                <td>{{ $u->name }}</td>
                <td>{{ $u->email }}</td>
                <td>{{ $u->role->title }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@else
    <div class="alert alert-danger" role="alert">
        Empty users!
    </div>
@endif

