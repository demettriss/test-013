<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    public function filials(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Company', 'parent_id', 'id');
    }

    public function users(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\User', 'company_id', 'id')->orderBy('name');
    }
}
