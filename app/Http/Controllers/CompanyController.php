<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function companies($uuid = null)
    {
        $company = Company::where('uuid', $uuid)->first();
        switch ($uuid) {
            case null:
                $companies = Company::whereNull('parent_id')->get();
                break;
            default:
                $companies = (!empty($company))
                    ? Company::where('parent_id', $company->id)->get()
                    : [];
        }
        $c = compact(
            'companies',
            'company'
        );
        return view('companies', $c)->render();
    }

    public function users(Request $request)
    {
        $company = Company::where('uuid', $request->get('uuid'))->first();
        return view('users', compact('company'))->render();
    }
}
