<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $arr = [];
        for ($i = 1; $i < 25; $i++) {
            for ($j = 0; $j < rand(5, 10); $j++) {
                $arr[] = [
                    'uuid' => (string) Str::uuid(),
                    'name' => $faker->name,
                    'email' => $faker->email,
                    'password' => Hash::make($faker->password),
                    'role_id' => rand(1, 2),
                    'company_id' => $i
                ];
            }
        }
        DB::table('users')->insert($arr);
    }
}
