<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $arr[] = [
            'uuid' => (string) Str::uuid(),
            'parent_id' => null,
            'title' => $faker->company,
            'desc' => $faker->text
        ];
        for ($i = 0; $i < 24; $i++) {
            $arr[] = [
                'uuid' => (string) Str::uuid(),
                'parent_id' => 1,
                'title' => $faker->company,
                'desc' => $faker->text
            ];
        }
        DB::table('companies')->insert($arr);
    }
}
